#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 23 09:48:40 2019

@author: Oz
"""

#!/usr/bin/env python
import pylab
import numpy as np
from numpy.random import random

#def mcarea(cenra, cendec, lim):
#    npoints=1000;
#    dmax=np.arccos(np.cos(np.deg2rad(-20))*np.cos(np.deg2rad(25))+np.sin(np.deg2rad(-20))*np.sin(np.deg2rad(25)))
#    isIn= 0
#    for i in range(npoints):
#        ramc = cenra-2*np.deg2rad(45)*(random()-0.5)
#        decmc =cendec-2*np.deg2rad(45)*(random()-0.5)
#        r = np.arccos(np.cos(cendec)*np.cos(decmc)*np.cos(cenra-ramc)+np.sin(cendec)*np.sin(decmc))
#        if ( (abs(r) <= dmax)*(decmc<np.deg2rad(25)) ):
#            isIn += 1
#    return isIn/npoints

def ki(x):
    return (np.cos(np.deg2rad(60))-np.sin(np.deg2rad(-35.15))*np.sin(x))/(np.cos(np.deg2rad(-35.15))*np.cos(x))
def funa(x):
    return np.piecewise(x,
                 [x>1,x<-1],
                 [lambda x: 0, 
                  lambda x: np.pi, 
                  lambda x: np.arccos(x)
                  ])
def expo(x):
    return(np.cos(np.deg2rad(-35))*np.cos(np.deg2rad(x))*np.sin(funa(ki(np.deg2rad(x))))
    +funa(ki(np.deg2rad(x)))*np.sin(np.deg2rad(-35))*np.sin(np.deg2rad(x)))


class Hist2D(object):

    def __init__(self, nxbins, xlow, xhigh, nybins, ylow, yhigh):
        self.nxbins = nxbins
        self.xhigh  = xhigh
        self.xlow   = xlow

        self.nybins = nybins
        self.yhigh  = yhigh
        self.ylow   = ylow

        self.nbins  = (nxbins, nybins)
        self.ranges = ((xlow, xhigh), (ylow, yhigh))

        self.hist, xedges, yedges = np.histogram2d([], [], bins=self.nbins, range=self.ranges)
        self.xbins = xedges
        self.ybins = yedges
        self.xcenters = (xedges[:-1] + xedges[1:]) / 2.
        self.ycenters = (yedges[:-1] + yedges[1:]) / 2.

    def fill(self, xarr, yarr):
        hist, _, _ = np.histogram2d(xarr, yarr, bins=self.nbins, range=self.ranges)
        self.hist += hist.T
        return None
    def expweight(self):
        for i in range(self.nxbins):
            for j in range(self.nybins):
                weight=np.cos(np.deg2rad(self.ycenters[j]))*expo(self.ycenters[j])
                self.hist[j][i]=self.hist[j][i]*(1/(weight+1))
        return None
    def smoothfill(self,ras,decl):
        #I want radians
        rasf=np.deg2rad(ras)
        decsf=np.deg2rad(decl)
#        counter=0
#        weights=1/(np.cos(decsf)*expo(decl))
        for i in range(self.nxbins):
            for j in range(self.nybins):
                #I want radians again
                ra0=np.deg2rad(self.xcenters[i])
                dec0=np.deg2rad(self.ycenters[j])
                
                #calculate all angular dists
                ang_dists=np.arccos(np.cos(dec0)*np.cos(decsf)*np.cos(ra0-rasf)+np.sin(dec0)*np.sin(decsf))
                if(self.ycenters[j]>-20):
                    self.hist[j][i]=sum( (abs(ang_dists) < np.deg2rad(45) )*(1/(0+1)) )
#                    self.hist[j][i]=sum( (abs(ang_dists) < np.deg2rad(45) )*(1/(0+1)) )

                    
                else: 
                    self.hist[j][i]=sum( (abs(ang_dists) < np.deg2rad(45) )*(1/(0+1)) )
#            counter=counter+1
#            print(counter)
#        return ang_dists
    def avgdw(self):
        decweight=[]
        for i in range(self.nybins):
            decweight.append(np.average(self.hist[i]))
            if(np.average(self.hist[i])>0):
                self.hist[i]=self.hist[i]/np.average(self.hist[i])
        
    def deltawsmoothfill(self,ras,decl,deltas):
        #I want radians
        rasf=np.deg2rad(ras)
        decsf=np.deg2rad(decl)
        weights=1/(np.cos(decsf)*expo(decl))
        for i in range(self.nxbins):
            for j in range(self.nybins):
                #I want radians again
                ra0=np.deg2rad(self.xcenters[i])
                dec0=np.deg2rad(self.ycenters[j])
                
                #calculate all angular dists
                ang_dists=np.arccos(np.cos(dec0)*np.cos(decsf)*np.cos(ra0-rasf)+np.sin(dec0)*np.sin(decsf))
                if(self.ycenters[j]>-20):
                    cut=(abs(ang_dists)<np.deg2rad(45))
                    self.hist[j][i]=(sum(weights[cut]*deltas[cut])/sum(weights[cut]))
#                    self.hist[j][i]=sum( (abs(ang_dits) < np.deg2rad(45) )*(1/(0+1)) )

                    
                else: 
                    cut=(abs(ang_dists)<np.deg2rad(45))
                    self.hist[j][i]=sum(weights[cut]*deltas[cut])/sum(weights[cut])
#            counter=counter+1
#            print(counter)
#        return ang_dists
    
    def deltaavgdw(self):
        
        for i in range(self.nybins):
            
            
            
            self.hist[i]=self.hist[i]/np.average(self.hist[i])
    
    @property
    def data(self):
        return self.xbins, self.ybins, self.hist





#if __name__ == "__main__":
#    from matplotlib import pyplot as plt
#
#    h = Hist1D(100, 0, 1)
#    for _ in range(1000):
#        a = np.random.random((3,))
#        h.fill(a)
#        plt.step(*h.data)
#        plt.show()
#
#    h = Hist2D(360, -180, 180, 180, -90, 90)
#    for _ in range(1000):
#        x, y = np.random.random((3,)), np.random.random((3,))
#        h.fill(x, y)
#        plt.pcolor(*h.data)
#        plt.show()

