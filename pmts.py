#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 24 11:31:40 2019

@author: Oz
"""
def find_nearest(a, a0):
    "Index of element in nd array `a` closest to the scalar value `a0`"
    idx = np.abs(a - a0).argmin()
    return idx
    
def tbench(θ,r):
    a0=-72
    a1=410
    b0=-0.049
    b1=0.36
    n0=-0.07
    n1=-1.14
    n2=0.84
    N=n0+n1/(np.cos(θ))**2+n2*np.exp(1/np.cos(θ))
    A=a0+a1*(np.cos(θ))**4
    B=b0+b1*(np.cos(θ))**4
    if r>800:
        return 40+N*np.sqrt(A**2+r*B**2)-A
    else:
        return 40+np.sqrt(A**2+r*B**2)-A

def TanKiFind(x,y, ang):
    return (-np.sin(ang)*x+y*np.cos(ang))/(x*np.cos(ang)+y*np.sin(ang))
    
    


import json
import glob
import numpy as np
from scipy.spatial.transform import Rotation as R
from scipy.integrate import cumtrapz

filelist=glob.glob('/home/Oz/LIP/SDData/*')
#filelist=glob.glob('[path to data folder]/*')
Δs=[]
indid=[]
rtt=[]
sigmat=[]
distaxist=[]
evenergy=[]
for n in range(0,len(filelist)):
    with open(filelist[n],encoding='utf-8', errors='replace') as filename:
            jdata=json.load(filename)
#    with open('/home/Oz/LIP/SDData/132675717300',encoding='utf-8', errors='replace') as filename:
#            jdata=json.load(filename)
    if(n%10000==0):
            print(n)

        
#indent  below
    
    
    jstat=jdata.get('stations')
    nbstat=jdata.get('nbstat')
#gather event data
    r=jdata.get('r')
    xev=jdata.get('x')
    yev=jdata.get('y')
    zev=jdata.get('z')
    theta=np.deg2rad(jdata.get('theta'))
    phi=np.deg2rad(jdata.get('phi'))
    ener=float(jdata.get('energy'))
    logener=np.log10(ener*(10**18))
    
    
#set counter for ignored tanks
    igctr=0
#gather tank data    
    xst=[]
    yst=[]
    zst=[]
    pmt1=[]
    pmt2=[]
    pmt3=[]
    atia=[]
    sigma=[]
    rt=[]
    Δi=[]

    for j in range(nbstat):
        
        #remove saturated
        signal=float(jstat[j].get('signal'))
        
        xst.append(xev-jstat[j].get('x'))
        yst.append(yev-jstat[j].get('y'))
        zst.append(zev-jstat[j].get('z'))
        distaxis = np.sqrt(xst[j]**2 + yst[j]**2)
        
        
        if( ( (logener>=19.6 and (distaxis<=2000) )or(logener>18.47 and logener<19.6 and (distaxis<=1400) )) and (signal>5 ) and ( int(jstat[j].get('extra').get('sat'))==0) and (distaxis>=300)  and 1/np.cos(theta)<1.45):
            sect.append(1/np.cos(theta))
            aops.append(jstat[j].get('extra').get('aop'))
            i=j-igctr
            pmt1.append( jstat[i].get('pmt1') )
            pmt2.append( jstat[i].get('pmt2') )
            pmt3.append( jstat[i].get('pmt3') )
            
    #accumulated trace integral average
            intsigs=[]
            if(np.trapz(pmt1[i])>0.5 and np.trapz(pmt1[i])<10000):
                intsigs.append(cumtrapz(pmt1[i])/cumtrapz(pmt1[i])[-1])
            if(np.trapz(pmt2[i])>0.5 and np.trapz(pmt2[i])<10000):
                intsigs.append(cumtrapz(pmt2[i])/cumtrapz(pmt2[i])[-1])
            if(np.trapz(pmt3[i])>0.5 and np.trapz(pmt3[i])<10000):
                intsigs.append(cumtrapz(pmt3[i])/cumtrapz(pmt3[i])[-1])
            
            atia.append(np.average(intsigs, axis=0 ))
    #calculate rise time (in bins). Each bin corresponds to 25ns
#            rt.append((25.0*(find_nearest(atia[i],0.5)-find_nearest(atia[i],0.1) ) ) )
            a=-3.9e-5
            b=-1.9e-5
            c=2.0e-4
            m=(a/np.cos(theta)+b/(np.cos(theta))**3+c)*np.sqrt(1/np.cos(theta)-1)
            g=m*(distaxis)**2
            ki=np.arctan(TanKiFind(xst[j],yst[j],phi))
            temprt=(25.0*(find_nearest(atia[i],0.5)-find_nearest(atia[i],0.1) ) )
            rt.append(temprt-g*np.cos(ki))
            
            rtt.append(temprt-g*np.cos(ki))
            
    #calc auxilliaries
            if (distaxis<=650):
                p0=-340+186/np.cos(theta)
                p1= 0.94-0.44/np.cos(theta)
            else:
                p0=-447+224/np.cos(theta)
                p1= 1.12-0.51/np.cos(theta)
                
            sigma.append(np.sqrt( ( ( p0+p1 )/np.sqrt(signal) )**2 + (np.sqrt(2)*25/np.sqrt(12))**2 ))
#            sigmat.append(np.sqrt( ( ( p0+p1 )/np.sqrt(signal) )**2 + (np.sqrt(2)*25/np.sqrt(12))**2 ))
            
            distaxist.append(distaxis)
            Δi.append(rt[i]-tbench(theta,distaxis)/sigma[i])
            evid.append(float(jdata.get('id')))

            
        else:
            igctr+=1
    if(nbstat-igctr>2):
        evenergy.append(ener)
        Δs.append(np.average(Δi))
        indid.append(float(jdata.get('id')))


indid=np.array(indid)
distaxist=np.array(distaxist)
rtt=np.array(rtt)
#//indent
