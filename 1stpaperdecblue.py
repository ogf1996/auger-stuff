#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 18 16:13:20 2019

@author: Oz
"""



import numpy as np
import pylab
def ga2equ(ga):
    """
    Convert Galactic to Equatorial coordinates (J2000.0)
    (use at own risk)
    
    Input: [l,b] in decimal degrees
    Returns: [ra,dec] in decimal degrees
    
    Source: 
    - Book: "Practical astronomy with your calculator" (Peter Duffett-Smith)
    - Wikipedia "Galactic coordinates"
    
    Tests (examples given on the Wikipedia page):
    >>> ga2equ([0.0, 0.0]).round(3)
    array([ 266.405,  -28.936])
    >>> ga2equ([359.9443056, -0.0461944444]).round(3)
    array([ 266.417,  -29.008])
    """
    l = np.deg2rad(ga[0])
    b = np.deg2rad(ga[1])

    # North galactic pole (J2000) -- according to Wikipedia
    pole_ra = np.deg2rad(180)
    pole_dec = np.deg2rad(27.128336)
    posangle = np.deg2rad(122.932-90.0)
    
    # North galactic pole (B1950)
    #pole_ra = radians(192.25)
    #pole_dec = radians(27.4)
    #posangle = radians(123.0-90.0)
    
    ra = np.arctan2( (np.cos(b)*np.cos(l-posangle)), (np.sin(b)*np.cos(pole_dec) - np.cos(b)*np.sin(pole_dec)*np.sin(l-posangle)) ) + pole_ra
    dec = np.arcsin( np.cos(b)*np.cos(pole_dec)*np.sin(l-posangle) + np.sin(b)*np.sin(pole_dec) )
    
    return np.array([np.rad2deg(ra)-180, np.rad2deg(dec)])

# read in public auger data
ids, n_stations, zeniths, azimuths, energies, timestamps, gal_lons, gal_lats = np.loadtxt("/home/Oz/LIP/Auger/auger_public.txt", unpack=True)

log_energies = np.log10(energies * 1.e18)  # calculate logarithm of energy
energies=10**log_energies
ra,dec=ga2equ([gal_lons,gal_lats])

#Guardar altura do histograma e range dos bins
N=30 #nº de bins
hg=pylab.hist(log_energies, bins=N, range=(17.5, 20.5), histtype='stepfilled') #guarda um vetor de vetores hg.

hval=hg[0]  #Guarda a altura do histograma
hvalk9=hval[9]
edges=10**hg[1] #Guarda os limites dos bins

avgEn=edges[0:N]+(edges[1:(N+1)]-edges[0:N])/2 #equivalente:avgEn=edges[1:(N+1)]+edges[0:N])/2: faz um vetor com os valores medios de energia de cada bin
avgLogEn=np.log10(avgEn) #faz os logaritmos de cada valor medio: tambem faz um vetor com os valores

exposure=57583.57*0.1 #por so termos 10% dos dados
norm=edges[1:(N+1)]-edges[0:N] #da o valor da largura de cada bin

#f=pylab.figure() #cria a figura para o grafico 
#ax=pylab.subplot(111) #localizadecr o grafico na figura
avgLogHE=avgLogEn[(avgLogEn>18.47)*(avgLogEn<20.2)] #vai nos so buscar os valores do vetor com logaritmos superiores a 18,47
avgHE=avgEn[(avgLogEn>18.47)*(avgLogEn<20.2)]
solangblack=1
fluxHE=0.9*((hval*(avgEn**3)/norm)[(avgLogEn>18.47)*(avgLogEn<20.2)])/(exposure) #da nos o valor do fluxo normalizado!!!(em parte: falta a exposiçao)
#pylab.yscale('log') #faz a escala logaritmica
#nomes dos eixos

#pylab.ylim(10**36,10**38)

error=0.9*((np.sqrt(hval)*(avgEn**3)/norm)[(avgLogEn>18.47)*(avgLogEn<20.2)])/exposure



pylab.figure()
#pylab.plot(avgLogHE, fluxHE,'ok',label='SD-1500 vertical')
pylab.errorbar(avgLogHE, fluxHE,yerr=error,fmt='ok',capsize=0, ms=3, elinewidth=1, ecolor='k', mew=1, mec='k',label='SD-1500 vertical', mfc='k')
pylab.yscale('log')
pylab.ylim(1e36,1e38)
pylab.xlabel('log(E)') 
pylab.ylabel('Flux')

print(fluxHE[9])
print(error[9])
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
# read in public auger data
ids, n_stations, zeniths, azimuths, energies, timestamps, gal_lons, gal_lats = np.loadtxt("/home/Oz/LIP/Auger/auger_public.txt", unpack=True)

log_energies = np.log10(energies * 1.e18)[(dec>-90)*(dec<-15.7)]  # calculate logarithm of energy
energies=10**log_energies

#Guardar altura do histograma e range dos bins
N=30 #nº de bins
hg=pylab.hist(log_energies, bins=N, range=(17.5, 20.5), histtype='stepfilled') #guarda um vetor de vetores hg.

hval=hg[0]  #Guarda a altura do histograma

edges=10**hg[1] #Guarda os limites dos bins

avgEn=edges[0:N]+(edges[1:(N+1)]-edges[0:N])/2 #equivalente:avgEn=edges[1:(N+1)]+edges[0:N])/2: faz um vetor com os valores medios de energia de cada bin
avgLogEn=np.log10(avgEn) #faz os logaritmos de cada valor medio: tambem faz um vetor com os valores

exposure=57583.57*0.1 #por so termos 10% dos dados
norm=edges[1:(N+1)]-edges[0:N] #da o valor da largura de cada bin

#f=pylab.figure() #cria a figura para o grafico 
#ax=pylab.subplot(111) #localizar o grafico na figura
avgLogHE=avgLogEn[(avgLogEn>18.47)*(avgLogEn<20.2)] #vai nos so buscar os valores do vetor com logaritmos superiores a 18,47
avgHE=avgEn[(avgLogEn>18.47)*(avgLogEn<20.2)]
solangblue=0.68
fluxHE=0.9*((hval*(avgEn**3)/norm)[(avgLogEn>18.47)*(avgLogEn<20.2)])/(exposure*solangblue) #da nos o valor do fluxo normalizado!!!(em parte: falta a exposiçao)
#pylab.yscale('log') #faz a escala logaritmica
#nomes dos eixos

#pylab.ylim(10**36,10**38)



#pylab.plot(avgLogHE, fluxHE,'sb',label='-90º<δ<-15.7º')

error=0.9*((np.sqrt(hval)*(avgEn**3)/norm)[(avgLogEn>18.47)*(avgLogEn<20.2)])/exposure

pylab.errorbar(avgLogHE, fluxHE,yerr=error,fmt='sb',capsize=0, ms=3, elinewidth=1, ecolor='b', mew=1, mec='b',label='-90º<δ<-15.7º', mfc='b')
pylab.yscale('log')
pylab.ylim(1e36,1e38)
pylab.xlabel('log(E)') 
pylab.ylabel('Flux')

###################################################r#####################################################
########################################################################################################
########################################################################################################
########################################################################################################

ids, n_stations, zeniths, azimuths, energies, timestamps, gal_lons, gal_lats = np.loadtxt("/home/Oz/LIP/Auger/auger_public.txt", unpack=True)


log_energies = np.log10(energies * 1.e18)[(dec>-15.7)*(dec<25)]  # calculate logarithm of energy
energies=10**log_energies

#Guardar altura do histograma e range dos bins
N=30 #nº de bins
hg=pylab.hist(log_energies, bins=N, range=(17.5, 20.5), histtype='stepfilled') #guarda um vetor de vetores hg.

hval=hg[0]  #Guarda a altura do histograma
hvalr9=hval[9]

edges=10**hg[1] #Guarda os limites dos bins

avgEn=edges[0:N]+(edges[1:(N+1)]-edges[0:N])/2 #equivalente:avgEn=edges[1:(N+1)]+edges[0:N])/2: faz um vetor com os valores medios de energia de cada bin
avgLogEn=np.log10(avgEn) #faz os logaritmos de cada valor medio: tambem faz um vetor com os valores

exposure=57583.57*0.1 #por so termos 10% dos dados
norm=edges[1:(N+1)]-edges[0:N] #da o valor da largura de cada bin

#f=pylab.figure() #cria a figura para o grafico 
#ax=pylab.subplot(111) #localizar o grafico na figura
avgLogHE=avgLogEn[(avgLogEn>18.47)*(avgLogEn<20.2)] #vai nos so buscar os valores do vetor com logaritmos superiores a 18,47
avgHE=avgEn[(avgLogEn>18.47)*(avgLogEn<20.2)]
solangred=0.32
fluxHE=0.9*((hval*(avgEn**3)/norm)[(avgLogEn>18.47)*(avgLogEn<20.2)])/(exposure*solangred) #da nos o valor do fluxo normalizado!!!(em parte: falta a exposiçao)
#pylab.yscale('log') #faz a escala logaritmica
#nomes dos eixos

#pylab.ylim(10**36,10**38)

error=0.9*((np.sqrt(hval)*(avgEn**3)/norm)[(avgLogEn>18.47)*(avgLogEn<20.2)])/exposure
pylab.errorbar(avgLogHE, fluxHE,yerr=error,fmt='^r',capsize=0, ms=3, elinewidth=1, ecolor='r', mew=1, mec='r',label='-15.7º<δ<25.0º', mfc='r')
#pylab.plot(avgLogHE, fluxHE,'^r', label='-15.7º<δ<25.0º')
pylab.yscale('log')
#pylab.ylim(1e36,1000e38)
pylab.xlim(18.5,20.25)
pylab.xlabel('log(E)') 
pylab.ylabel('Flux')
pylab.legend()

print(fluxHE[9])
print(error[9])

pylab.savefig('paper1_decwindows.png', dpi=1000)