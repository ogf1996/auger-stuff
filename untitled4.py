#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 24 11:31:40 2019

@author: Oz
"""
import json
import glob

filelist=glob.glob('/home/Oz/LIP/SDData/*')
masses=[]

for file in filelist:
    jdata=json.load(open(file))
    jstat=jdata.get('stations')
    nbstat=jdata.get('nbstat')
    pmt1=[]
    pmt2=[]
    pmt3=[]
    for i in range(nbstat):
        pmt1.append(jstat[i].get('pmt1'))
        pmt2.append(jstat[i].get('pmt2'))
        pmt3.append(jstat[i].get('pmt3'))
    
    Δs=(1/nbstations)
    