#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 26 09:15:49 2019

@author: Oz
"""

#deltas=np.zeros(len(ids))-100000
#log_energies = np.log10(energies * 1.e18)  # calculate logarithm of energy
#for k in range((len(indid))):
#    deltas[np.where(ids==indid[k])]=Δs[k]
#    if(k%10000==0):
#        print(k)
    
#%%
###################################
#    Calcular <Δs>
###################################
N=20
#del hg
#del hgw
pylab.figure()
log_energies = np.log10(np.array(energies) * 1.e18)  # calculate logarithm of energy
hg=pylab.hist(log_energies, bins=N, range=(18.5, 20.5), histtype='stepfilled')
pylab.title('Histograma do nº de eventos \npor bin de logaritmo de energia')
hval=hg[0]
edges=10**hg[1]

avgEn=edges[0:N]+(edges[1:(N+1)]-edges[0:N])/2 
avgLogEn=np.log10(avgEn)
norm=edges[1:(N+1)]-edges[0:N]

pylab.figure()
pylab.title('Histograma do nº de eventos pesados \npelo valor de Δs respetivo')
hgw=pylab.hist(log_energies, bins=N, weights=deltas, range=(18.5, 20.5), histtype='stepfilled')

pylab.figure()
pylab.title('Valor dos bins do histograma pesado\n dividido por nº de eventos no bin')
avgΔs=hgw[0][avgLogEn>18.47]/hg[0][avgLogEn>18.47]
(a,b)=np.polyfit(avgLogEn[avgLogEn>18.47][:-2], avgΔs[:-2]/avgΔs[4]-1, 1)
pylab.plot(log_energies,deltas, 'o', ms=1)
pylab.plot(avgLogEn[avgLogEn>18.47], (avgΔs), 'sk')
x_d = np.linspace(18.4, 22.2, 2)
yp = 1*x_d-18.39
dyp = 0.974*x_d-18.08
yf=1.06*x_d-20.47
dyf=0.915*x_d-18.09
#pylab.plot(xs, a*xs+b,'-r')


pylab.plot(x_d, yp,'-r')
pylab.plot(x_d, dyp,'--r')
pylab.plot(x_d, yf,'-b')
pylab.plot(x_d, dyf,'--b')


pylab.ylim(-1.5,2)
pylab.xlim(18.5,20.25)