#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 23 11:50:00 2019

@author: Oz
"""
import numpy as np

ids, n_stations, zeniths, azimuths, energies, timestamps, gal_lons, gal_lats = np.loadtxt("auger_public.txt", unpack=True)

#%%
ecut=(energies>=32)                     #Definir cortes em energia

nbins=90                                #Definir nº de bins (em cada direçao)
ra_min,ra_max=[-180,180]
dec_min,dec_max=[-90,25]

h = Hist2D(nbins, ra_min, ra_max, dec_min, dec_max)   #Criar histograma

ra,dec=ga2equ([gal_lons,gal_lats])      #Obter coords equatoriais



h.smoothfill(ra[ecut],dec[ecut])        #Preencher histograma, utilizando smoothing
h.avgdw()                               #Dividir cada bin pelo nº de eventos na banda de declinaçao


#%%Criar um novo histograma em radianos, para representaçao na projeçao 'hammer'
hrad=h
hrad.xbins=np.deg2rad(h.xbins)
hrad.ybins=np.deg2rad(h.ybins)
####

#%%Plotting
pylab.figure();
pylab.subplot(111, projection='hammer');
pylab.pcolor(*hrad.data,cmap='RdYlBu_r', vmin=np.min(h.hist),vmax=np.max(h.hist)); #Desenha o histograma. Escala de cor retirada dos 
pylab.title('Titulo \n')
pylab.colorbar()
pylab.grid()
#pylab.savefig('E_over_8_Delta_under_0.png', dpi=1000) #Guarda imagem, se necessario
