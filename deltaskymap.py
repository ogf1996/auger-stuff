#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 23 11:50:00 2019

@author: Oz
"""

import pylab
import numpy as np

#del h2
h2 = Hist2D(90, -180, 180, -82,12)
ids, n_stations, zeniths, azimuths, energies, timestamps, gal_lons, gal_lats,ra,dec,deltas = np.loadtxt("/home/Oz/LIP/Auger/deltaevents.txt", unpack=True)
log_energies=np.log10(energies*10**18)

LLimProt = 0.974*log_energies-18.08
ULimFe = 1.06*log_energies-20.47

ecut=(deltas<ULimFe)*(energies>16)

h2.smoothfill(ra[ecut],dec[ecut])
evbins=h2.hist

sigma=1/(np.sqrt(np.average(h2.hist)))
N=np.average(h2.hist)

del h2
h2 = Hist2D(90, -180, 180, -82,12)
h2.deltawsmoothfill(ra[ecut],dec[ecut],deltas[ecut])

sigma2=abs(sigma*np.average(h2.hist))
print(f'\nAvg N by bin is {N:f}\nσ={sigma*100:.1f}%\nAvg val by bin is {np.average(h2.hist):.2f}\nδ={sigma2:.3f}\n')
#h2.deltaavgdw()

#pylab.figure()
#pylab.pcolor(*h2.data,cmap='RdYlBu_r',vmin=0,vmax=1.83)
#pylab.colorbar()

#pylab.plot(ra[(energies>ecut)],dec[(energies>ecut)],'sk', ms=1)

hrad=h2
hrad.xbins=np.deg2rad(h2.xbins)
hrad.ybins=np.deg2rad(h2.ybins)
pylab.figure();
pylab.subplot(111, projection='hammer');
pylab.pcolor(*hrad.data,cmap='RdYlBu_r', vmin=np.min(h2.hist),vmax=np.max(h2.hist));
#pylab.plot(np.deg2rad(ra)[ecut],np.deg2rad(dec)[ecut], 'sk', ms=2)
#pylab.title('All events\n')
pylab.colorbar()
pylab.grid()
pylab.figure()
pylab.hist(h2.hist.flatten(), bins=np.arange(float(f'{np.min(h2.hist):.1f}'),float(f'{np.max(h2.hist):.2f}')+sigma2,sigma2))
pylab.title('Distribution of bin values\n')

#pylab.xticks(np.arange(float(f'{np.min(h2.hist):.1f}'),np.max(h2.hist),sigma))
#pylab.savefig('E_over_8_Delta_under_0.png', dpi=1000)