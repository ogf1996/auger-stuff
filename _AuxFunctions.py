#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Criado por Pedro Branco, Osvaldo Freitas e Pedro Passos no ambito dos estagios de verao do LIP-Minho 2019.
Projeto orientado por Raul Sarmento

Este ficheiro define um nº de funçoes necessarias para o funcionamente das outras rotinas.
"""


#!/usr/bin/env python

import numpy as np

def ga2equ(ga): #define funçao para converter coordenadas galaticas em equatoriais

    l = np.deg2rad(ga[0])
    b = np.deg2rad(ga[1])

    # North galactic pole (J2000) -- according to Wikipedia
    pole_ra = np.deg2rad(192.25)
    pole_dec = np.deg2rad(27.128336)
    posangle = np.deg2rad(122.932-90.0)
    
    ra = np.arctan2( (np.cos(b)*np.cos(l-posangle)), (np.sin(b)*np.cos(pole_dec) - np.cos(b)*np.sin(pole_dec)*np.sin(l-posangle)) ) + pole_ra
    dec = np.arcsin( np.cos(b)*np.cos(pole_dec)*np.sin(l-posangle) + np.sin(b)*np.sin(pole_dec) )
    
    return np.array([np.rad2deg(ra), np.rad2deg(dec)])

def ki(x):
    return (np.cos(np.deg2rad(60))-np.sin(np.deg2rad(-35.15))*np.sin(x))/(np.cos(np.deg2rad(-35.15))*np.cos(x))
def funa(x):
    return np.piecewise(x,
                 [x>1,x<-1],
                 [lambda x: 0, 
                  lambda x: np.pi, 
                  lambda x: np.arccos(x)
                  ])
def expo(x):
    return(np.cos(np.deg2rad(-35))*np.cos(np.deg2rad(x))*np.sin(funa(ki(np.deg2rad(x))))
    +funa(ki(np.deg2rad(x)))*np.sin(np.deg2rad(-35))*np.sin(np.deg2rad(x)))


class Hist2D(object):

    def __init__(self, nxbins, xlow, xhigh,nybins, ylow, yhigh):
        self.nxbins = nxbins
        self.xhigh  = xhigh
        self.xlow   = xlow

        self.nybins = nybins
        self.yhigh  = yhigh
        self.ylow   = ylow

        self.nbins  = (nxbins, nybins)
        self.ranges = ((xlow, xhigh), (ylow, yhigh))

        self.hist, xedges, yedges = np.histogram2d([], [], bins=self.nbins, range=self.ranges)
        self.xbins = xedges
        self.ybins = yedges
        self.xcenters = (xedges[:-1] + xedges[1:]) / 2.
        self.ycenters = (yedges[:-1] + yedges[1:]) / 2.

    def fill(self, xarr, yarr):
        hist, _, _ = np.histogram2d(xarr, yarr, bins=self.nbins, range=self.ranges)
        self.hist += hist.T
        return None
    def expweight(self):
        for i in range(self.nxbins):
            for j in range(self.nybins):
                weight=np.cos(np.deg2rad(self.ycenters[j]))*expo(self.ycenters[j])
                self.hist[j][i]=self.hist[j][i]*(1/(weight+1))
        return None
    def smoothfill(self,ras,decl):
        #I want radians
        rasf=np.deg2rad(ras)
        decsf=np.deg2rad(decl)

        for i in range(self.nxbins):
            for j in range(self.nybins):
                #I want radians again
                ra0=np.deg2rad(self.xcenters[i])
                dec0=np.deg2rad(self.ycenters[j])
                
                #calculate all angular dists
                ang_dists=np.arccos(np.cos(dec0)*np.cos(decsf)*np.cos(ra0-rasf)+np.sin(dec0)*np.sin(decsf))
                if(self.ycenters[j]>-20):
                    self.hist[j][i]=sum( (abs(ang_dists) < np.deg2rad(45) )*(1/(0+1)) )
                else: 
                    self.hist[j][i]=sum( (abs(ang_dists) < np.deg2rad(45) )*(1/(0+1)) )

    def avgdw(self):
        for i in range(self.nybins):
            self.hist[i]=self.hist[i]/np.average(self.hist[i])
        
    def deltawsmoothfill(self,ras,decl,deltas):
        #I want radians
        rasf=np.deg2rad(ras)
        decsf=np.deg2rad(decl)
        weights=1/(np.cos(decsf)*expo(decl))
        for i in range(self.nxbins):
            for j in range(self.nybins):
                #I want radians again
                ra0=np.deg2rad(self.xcenters[i])
                dec0=np.deg2rad(self.ycenters[j])
                
                #calculate all angular dists
                ang_dists=np.arccos(np.cos(dec0)*np.cos(decsf)*np.cos(ra0-rasf)+np.sin(dec0)*np.sin(decsf))
                
                if(self.ycenters[j]>-20):
                    cut=(abs(ang_dists)<np.deg2rad(45))
                    self.hist[j][i]=(sum(weights[cut]*deltas[cut])/sum(weights[cut]))
                else: 
                    cut=(abs(ang_dists)<np.deg2rad(45))
                    self.hist[j][i]=sum(weights[cut]*deltas[cut])/sum(weights[cut])

    
    @property
    def data(self):
        return self.xbins, self.ybins, self.hist

